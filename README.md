# Learn the basic Shading

This basic shading is perhaps the easiest sketching you have ever seen. Though this art is simple as you have seen, if you are learning the basic, this art is right for you. The basic shading always starts on sketching: line, point, movement, emotions and gender. 

See the difference of each subject and learn the basic analysis of each line. If you prefer the gender, learn the body shape and standing of each subject. Using the [bespoke mens suits online](http://harrisandzei.com), advance digital sketching and pencil on paper, you can make your own art in no time. 

![Screenshot of Asteroids](http://comicbookgraphicdesign.com/wp-content/uploads/2014/09/comic-art-reference-anatomy-basic-shapes-PREVIEW.jpg)

![Screenshot of Asteroids](http://2.bp.blogspot.com/_SP_kLrzk_Ts/TLH01XINRWI/AAAAAAAAAfs/z5RQUF0th60/s1600/poses.jpg)

![Screenshot of Asteroids](http://img09.deviantart.net/bedb/i/2009/032/2/c/draw_basic_people_part_2_by_ditroi.jpg)